<?php

namespace App\Controller;

use App\Entity\MicroPost;
use App\Entity\User;
use App\Form\MicroPostType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route("/micro-post")
 */
class MicroPostController
{

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var \App\Repository\MicroPostRepository
     */
    private $micro_post_repository;

    /**
     * @var \Symfony\Component\Form\FormFactoryInterface
     */
    private $form_factory;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $entity_manager;

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\SessionBagInterface
     */
    private $session_bag;

    public function __construct(
        RouterInterface $router,
        \Twig_Environment $twig,
        FormFactoryInterface $form_factory,
        EntityManagerInterface $entity_manager,
        \App\Repository\MicroPostRepository $micro_post_repository,
        FlashBagInterface $session_bag
    )
    {
        $this->twig = $twig;
        $this->router = $router;
        $this->form_factory = $form_factory;
        $this->entity_manager = $entity_manager;
        $this->micro_post_repository = $micro_post_repository;
        $this->session_bag = $session_bag;
    }

    /**
     * @Route("/", name="micro_post_index")
     */
    public function index(TokenStorageInterface $token_storage, UserRepository $user_repository)
    {
        $current_user = $token_storage->getToken()->getUser();
        $users_to_follow = [];
        if ($current_user instanceof User) {
            $posts = $this->micro_post_repository->findAllByUsers(
                $current_user->getFollowing()
            );
            $users_to_follow = count($posts) === 0
                ? $user_repository->findAllWithMoreThenFivePostsExceptUser($current_user)
                : [];
        }
        else {
            $posts = $this->micro_post_repository->findBy([], ['date' => 'DESC']);
        }
        $html = $this->twig->render(
            'micro-post/index.html.twig', [
                'posts' => $posts,
                'users_to_follow' => $users_to_follow,
            ]
        );

        return new Response($html);
    }

    /**
     * @Route("/edit/{id}", name="micro_post_edit")
     * @Security("is_granted('edit', post)", message="Access denied")
     */
    public function edit(MicroPost $post, Request $request)
    {
        $form = $this->form_factory->create(MicroPostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entity_manager->flush();

            return new RedirectResponse($this->router->generate('micro_post_index'));
        }
        return new Response(
            $this->twig->render('micro-post/add.html.twig', [
                'form' => $form->createView(),
            ])
        );
    }

    /**
     * @Route("/delete/{id}", name="micro_post_delete")
     * @Security("is_granted('delete', post)", message="Access denied")
     */
    public function delete(MicroPost $post)
    {
        $this->entity_manager->remove($post);
        $this->entity_manager->flush();
        $this->session_bag->add('notice', 'Micro post was deleted');

        return new RedirectResponse($this->router->generate('micro_post_index'));
    }

    /**
     * @Route("/add", name="micro_post_add")
     * @Security("is_granted('ROLE_USER')")
     */
    public function add(Request $request, TokenStorageInterface $token_storage)
    {
        $user = $token_storage->getToken()->getUser();
        $micro_post = new MicroPost();
//        $micro_post->setDate(new \DateTime());
        $micro_post->setUser($user);
        $form = $this->form_factory->create(MicroPostType::class, $micro_post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entity_manager->persist($micro_post);
            $this->entity_manager->flush();

            return new RedirectResponse($this->router->generate('micro_post_index'));
        }
        return new Response(
            $this->twig->render('micro-post/add.html.twig', [
                'form' => $form->createView(),
            ])
        );
    }

    /**
     * @Route("/{id}", name="micro_post_post")
     */
    public function post(MicroPost $post)
    {
        return new Response(
            $this->twig->render('micro-post/post.html.twig', [
                'post' => $post,
            ])
        );
    }

    /**
     * @Route(path="/user/{username}", name="micro_post_user")
     */
    public function userPosts(User $userWithPosts)
    {
        $html = $this->twig->render('micro-post/user-posts.html.twig', [
            'posts' => $this->micro_post_repository->findBy(['user' => $userWithPosts], [
                'date' => 'DESC',
            ]),
            'user' => $userWithPosts,
        ]);

        return new Response($html);
    }

}
