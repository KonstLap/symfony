<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/following")
 */
class FollowingController extends Controller
{

    /**
     * @Route("/follow/{id}", name="following_follow")
     */
    public function follow(User $user_to_follow)
    {
        /** @var User $current_user  */
        $current_user = $this->getUser();

        if ($current_user->getId() !== $user_to_follow->getId()) {
            $current_user->follow($user_to_follow);
            $this->getDoctrine()
                 ->getManager()
                 ->flush();
        }

        return $this->redirectToRoute(
            'micro_post_user',
            ['username' => $user_to_follow->getUsername()]
        );
    }

    /**
     * @Route("/unfollow/{id}", name="following_unfollow")
     */
    public function unfollow(User $user_to_unfollow)
    {
        /** @var User $current_user  */
        $current_user = $this->getUser();

        if ($current_user->getId() !== $user_to_unfollow->getId()) {
            $current_user->getFollowing()
                         ->removeElement($user_to_unfollow);
            $this->getDoctrine()
                 ->getManager()
                 ->flush();
        }

        return $this->redirectToRoute(
            'micro_post_user',
            ['username' => $user_to_unfollow->getUsername()]
        );
    }

}
