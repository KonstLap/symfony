<?php

namespace App\Controller;


use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController
{

    /**
     * @var \Twig_Environment
     */
    private $twig;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/login", name="security_login")
     */
    public function login(AuthenticationUtils $authentication_utils)
    {
        return new Response(
            $this->twig->render('security/login.html.twig', [
                'last_username' => $authentication_utils->getLastUsername(),
                'error' => $authentication_utils->getLastAuthenticationError(),
            ])
        );
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logout()
    {

    }

    /**
     * @Route("/confirm/{token}", name="security_confirm")
     */
    public function confirm(
        string $token,
        UserRepository $user_repository,
        EntityManagerInterface $entity_manager
    ) {
        $user = $user_repository->findOneBy([
            'confirmation_token' => $token,
        ]);

        if ($user !== null) {
            $user->setEnabled(true);
            $user->setConfirmationToken('');

            $entity_manager->flush();
        }

        return new Response($this->twig->render('security/confirmation.html.twig', [
            'user' => $user,
        ]));
    }
}
