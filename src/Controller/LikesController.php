<?php

namespace App\Controller;

use App\Entity\MicroPost;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/likes")
 */
class LikesController extends Controller
{

    /**
     * @Route("/like/{id}", name="likes_like")
     */
    public function like(MicroPost $micro_post)
    {
        /** @var User $current_user */
        $current_user = $this->getUser();

        if (!$current_user instanceof User) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }
        $micro_post->like($current_user);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
           'count' => $micro_post->getLikedBy()->count(),
        ]);
    }

    /**
     * @Route("/unlike/{id}", name="likes_unlike")
     */
    public function unlike(MicroPost $micro_post)
    {
        /** @var User $current_user */
        $current_user = $this->getUser();

        if (!$current_user instanceof User) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }
        $micro_post->getLikedBy()->removeElement($current_user);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'count' => $micro_post->getLikedBy()->count(),
        ]);
    }

}
