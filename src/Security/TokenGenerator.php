<?php

namespace App\Security;

class TokenGenerator
{

    private const ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghifklmnopqrstuvwxyz1234567890';

    public function getRandomSecureToken(int $length)
    {
        $max_number = strlen(self::ALPHABET);
        $token = '';

        for ($i = 0; $i < $length; $i++) {
            $token .= self::ALPHABET[random_int(0, $max_number - 1)];
        }

        return $token;
    }

}
