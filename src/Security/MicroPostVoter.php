<?php

namespace App\Security;


use App\Entity\MicroPost;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MicroPostVoter extends Voter
{

    const EDIT = 'edit';
    const DELETE = 'delete';

    /**
     * @var \Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface
     */
    private $decision_manager;


    public function __construct(AccessDecisionManagerInterface $decision_manager)
    {

        $this->decision_manager = $decision_manager;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::DELETE, self::EDIT])) {
            return false;
        }

        if (!$subject instanceof MicroPost) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if ($this->decision_manager->decide($token, [User::ROLE_ADMIN])) {
            return true;
        }

        $authenticated_user = $token->getUser();

        if (!$authenticated_user instanceof User) {
            return false;
        }

        /** @var MicroPost $micro_post */
        $micro_post = $subject;
        return $micro_post->getUser()->getId() === $authenticated_user->getId();
    }
}
