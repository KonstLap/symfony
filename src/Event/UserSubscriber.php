<?php

namespace App\Event;

use App\Entity\UserPreferences;
use App\Mailer\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserSubscriber implements EventSubscriberInterface
{

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $entity_manager;

    /**
     * @var string
     */
    private $default_locale;

    public function __construct(
        Mailer $mailer,
        EntityManagerInterface $entity_manager,
        string $default_locale
    ) {
        $this->mailer = $mailer;
        $this->entity_manager = $entity_manager;
        $this->default_locale = $default_locale;
    }

    public static function getSubscribedEvents()
    {
        return [
            UserRegisterEvent::NAME => 'onUserRegister',
        ];
    }

    public function onUserRegister(UserRegisterEvent $user_register_event)
    {
        $preferences = new UserPreferences();
        $preferences->setLocale($this->default_locale);

        $user = $user_register_event->getRegisteredUser();
        $user->setPreferences($preferences);

        $this->entity_manager->persist($preferences);
        $this->entity_manager->flush();

        $this->mailer->sendConfirmationMessage($user_register_event->getRegisteredUser());
    }

}
