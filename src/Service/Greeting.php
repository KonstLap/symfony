<?php


namespace App\Service;


use Psr\Log\LoggerInterface;

class Greeting
{

  /**
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * Greeting constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * @param string $name
   *
   * @return string
   */
  public function greet(string $name)
  {
    $this->logger->info("Greeted $name");
    return "Hello $name";
  }

}
