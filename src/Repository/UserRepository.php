<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findAllWithMoreThenFivePosts()
    {

        return $this->getAllWithMoreThenFivePostsQuery()
            ->getQuery()
            ->getResult();
    }

    public function findAllWithMoreThenFivePostsExceptUser(User $user)
    {
        return $this->getAllWithMoreThenFivePostsQuery()
            ->andHaving('u != :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    private function getAllWithMoreThenFivePostsQuery() : QueryBuilder
    {
        $qb = $this->createQueryBuilder('u');
        return $qb->select('u')
                  ->innerJoin('u.posts', 'mp')
                  ->groupBy('u')
                  ->having('COUNT(mp) > 5');
    }

}
