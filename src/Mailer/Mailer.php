<?php

namespace App\Mailer;


use App\Entity\User;
use App\Event\UserRegisterEvent;

class Mailer
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var string
     */
    private $email_from;

    public function __construct(
        \Swift_Mailer $mailer,
        \Twig_Environment $twig,
        string $email_from
    ) {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->email_from = $email_from;
    }

    public function sendConfirmationMessage(User $user)
    {
        $body = $this->twig->render('email/registration.html.twig', [
            'user' => $user,
        ]);

        $message = (new \Swift_Message())
            ->setFrom($this->email_from)
            ->setSubject('Welcome to the Micro post app!')
            ->setBody($body, 'text/html')
            ->setTo($user->getEmail());

        $this->mailer->send($message);
    }

}
