<?php

namespace App\EventListener;

use App\Entity\LikeNotification;
use App\Entity\MicroPost;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\PersistentCollection;

class LikeNotificationSubscriber implements EventSubscriber
{

    public function getSubscribedEvents()
    {
        return [
           Events::onFlush,
        ];
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        /** @var PersistentCollection $collection_update */
        foreach ($uow->getScheduledCollectionUpdates() as $collection_update) {
            if (!$collection_update->getOwner() instanceof MicroPost) {
                continue;
            }

            if ($collection_update->getMapping()['fieldName'] !== 'liked_by') {
                continue;
            }

            $insert_diff = $collection_update->getInsertDiff();

            if (!count($insert_diff)) {
                return;
            }

            /** @var MicroPost $micro_post */
            $micro_post = $collection_update->getOwner();

            $notification = new LikeNotification();
            $notification->setUser($micro_post->getUser());
            $notification->setMicroPost($micro_post);
            $notification->setLikedBy(reset($insert_diff));

            $em->persist($notification);

            $uow->computeChangeSet(
                $em->getClassMetadata(LikeNotification::class),
                $notification
            );
        }
    }

}
