<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LikeNotificationRepository")
 */
class LikeNotification extends Notification
{

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MicroPost")
     */
    private $micro_post;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $liked_by;

    /**
     * @return mixed
     */
    public function getMicroPost()
    {
        return $this->micro_post;
    }

    /**
     * @param mixed $micro_post
     */
    public function setMicroPost($micro_post)
    {
        $this->micro_post = $micro_post;
    }

    /**
     * @return mixed
     */
    public function getLikedBy()
    {
        return $this->liked_by;
    }

    /**
     * @param mixed $liked_by
     */
    public function setLikedBy($liked_by)
    {
        $this->liked_by = $liked_by;
    }

}
