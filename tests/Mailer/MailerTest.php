<?php

namespace App\Tests\Mailer;

use App\Entity\User;
use App\Mailer\Mailer;
use PHPUnit\Framework\TestCase;

class MailerTest extends TestCase
{

    public function testSendingConfirmationMessage()
    {
        $user = new User();
        $user->setEmail('boom@bam.com');

        $swift_mailer_mock = $this->getMockBuilder(\Swift_Mailer::class)
            ->disableOriginalConstructor()
            ->getMock();
        $swift_mailer_mock->expects($this->once())
            ->method('send')
            ->with($this->callback(function ($subject) {
                $message_string = (string) $subject;

                return strpos($message_string, 'Subject: Welcome to the Micro post app!')
                    && strpos($message_string, 'From: sample@me.com')
                    && strpos($message_string, 'To: boom@bam.com')
                    && strpos($message_string, 'Content-Type: text/html; charset=utf-8')
                    && strpos($message_string, 'This is a message body! Yeah!');
            }));

        $twig_mock = $this->getMockBuilder(\Twig_Environment::class)
            ->disableOriginalConstructor()
            ->getMock();
        $twig_mock->expects($this->once())
            ->method('render')
            ->with('email/registration.html.twig', [
                'user' => $user,
            ])
            ->willReturn('This is a message body! Yeah!');

        $mailer = new Mailer($swift_mailer_mock, $twig_mock, 'sample@me.com');
        $mailer->sendConfirmationMessage($user);
    }

}
