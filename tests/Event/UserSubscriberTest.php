<?php

namespace App\Tests\Event;

use App\Entity\User;
use App\Entity\UserPreferences;
use App\Event\UserRegisterEvent;
use App\Event\UserSubscriber;
use App\Mailer\Mailer;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

class UserSubscriberTest extends TestCase
{

    public function testUserSubscriber()
    {
        $preferences = new UserPreferences();
        $preferences->setLocale('en');

        $user = new User();
        $user->setEmail('me@me.com');
        $user->setPreferences($preferences);

        $user_register_event = new UserRegisterEvent($user);

        $em = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $em->expects($this->once())
            ->method('persist')
            ->with($preferences)
            ->willReturn($this->isTrue());
        $em->expects($this->once())
            ->method('flush')
            ->willReturn($this->isTrue());

        $mailer = $this->getMockBuilder(Mailer::class)->disableOriginalConstructor()->getMock();
        $mailer->expects($this->once())
            ->method('sendConfirmationMessage')
            ->with($user)
            ->willReturn($this->isTrue());

        $user_subscriber = new UserSubscriber($mailer, $em, 'en');

        $subscriber_event = $user_subscriber::getSubscribedEvents();

        $user_subscriber->onUserRegister($user_register_event);

        $this->assertEquals([UserRegisterEvent::NAME => 'onUserRegister'], $subscriber_event);
    }

}
