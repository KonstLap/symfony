<?php

namespace App\Tests\Event;

use App\Entity\User;
use App\Event\UserRegisterEvent;
use PHPUnit\Framework\TestCase;

class UserRegisterEventTest extends TestCase
{

    public function testUserRegisterEvent()
    {
        $user = new User();
        $user_register_event = new UserRegisterEvent($user);
        /** @var User $registered_user */
        $registered_user = $user_register_event->getRegisteredUser();

        $this->assertEquals(User::class, get_class($registered_user));
    }

}
