<?php

namespace App\Tests\Security;


use App\Security\TokenGenerator;
use PHPUnit\Framework\TestCase;

class TokenGeneratorTest extends TestCase
{

    public function testTokenGeneration()
    {
        $token_generator = new TokenGenerator();
        $token = $token_generator->getRandomSecureToken(25);

        $this->assertTrue(ctype_alnum($token));
        $this->assertEquals(25, strlen($token));
    }

}
